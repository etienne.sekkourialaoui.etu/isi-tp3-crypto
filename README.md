Binome: Etienne SEKKOURI ALAOUI

Prologue: On dispose de fichier simulants des clés USB allant de A à D, un dossier ramdisk simulant une sorte de passerelle de stockage, et un dossier disk ou se trouve le fichier des paires nom numéro, ici rempli avec du texte.
Il y a un dossier par couple de question.

Q1: On va utiliser le principe d'authentification par clé cryptée. La clé sera générée puis coupée en deux. Un partie est mise dans une clé usb et l'autre partie dans l'autre clé.

Pour crypter ou décrypter un fichier, on recrée la clé précedement générée en récupérant les parties sur les deux clés usb. on décrypte ou encrypte ensuite avec la clé reformée.

Pour l'ajout de couple, on crée la clé maitre, on décrypte , on rajoute la ligne de texte dans le fichier, on recrypte le fichier et on supprime les différents fichiers de clé qui ne sont pas dans une clé usb.

Pour le retrait, on crée la clé maitre, on décrypte , on réécrit dans un nouveau fichier le contenu du fichier ligne par ligne tant que la ligne n'est pas celle qu'on veut supprimer. On supprime l'ancien fichier, on change le nom du nouveau, on recrypte et on supprime fichiers de clé qui ne sont pas dans une clé usb.

Q2: L'ajout ne marche pas car je n'arrive pas à mettre la chaine de caractère désirée.
Pareil pour la suppression.
Si il y a une erreur de mot de passe, le fichier se fait supprimer, je ne sias pas ocmment gérer cela.

Q3: On va juste faire une copie de chaque partie de la clé dans une autre clé usb, celle des représentant.
Lorsqu'on aura besoin d'une partie de clé, on va vérifier dans les deux clés usb pour chaque partie. Si il n'y a pas une clé usb, l'autre représentant prend le relais automatiquement.
Tout se passe ensuite comme si il n'y avait pas de représentant supplémentaire.

Q4: Même problèmes qu'à Q2.
Il faut rentrer le mot de passe de cryptage pour chaque représentant présent.
